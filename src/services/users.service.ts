import { API_URL} from './env.service'
import axios from 'axios'
import { Toast } from '../toast'

import { SetSessionToken, SetSessionUser} from '../session'

export async function CurrentUser(token: string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/users/me', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {                            
                resolve(response.data)
            }).catch(error => {                            
                resolve(false)
            })
    })
    
}


export async function addUser(token: string, body : any): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.post(API_URL + '/users', body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

export async function ListUsers(token: string): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/users', { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => { 
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}


export async function DetailUser(token: string, id : any): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/users/'+ id, { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => {                                           
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}


export async  function LogoutUser(){         
    SetSessionToken(null)
    SetSessionUser(null)
    Toast("Déconnection avec succés")
}

export async function deleteUser(token: string, id : number): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.delete(API_URL + '/users/'+ id ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}
export async function updateUser(token: string, id:number, body : any): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.put(API_URL + '/users/'+ id, body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}