import { API_URL} from './env.service'
import axios from 'axios'


export async function getListRoles(token: string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/roles', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {   
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function addRole(token: string, body : any): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.post(API_URL + '/roles', body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

export async function detailRole(token: string, id : any): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/roles/'+ id, { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => {                                         
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function deleteRole(token: string, id : string): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.delete(API_URL + '/roles/'+ id ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

export async function updateRole(token: string, id:string, name:string, description:string): Promise<any> {
    var body = {
        name: name,
        description : description
    }
    return new Promise((resolve, reject) => {
    axios.put(API_URL + '/roles/'+ id, body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

