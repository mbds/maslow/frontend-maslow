import { API_URL} from './env.service'
import axios from 'axios'


export async function getListPrivileges(token: string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/privileges', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {   
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function addPrivilege(token: string, body : any): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.post(API_URL + '/privileges', body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

export async function detailPrivilege(token: string, id : any): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/privileges/'+ id, { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => {                               
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function deletePrivilege(token: string, id : string): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.delete(API_URL + '/privileges/'+ id ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

export async function updatePrivilege(token: string, id:string, name:string, description:string): Promise<any> {
    var body = {
        name: name,
        description : description
    }
    return new Promise((resolve, reject) => {
    axios.put(API_URL + '/privileges/'+ id, body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

