import { API_URL, HOST, PORT } from './env.service'
import axios from 'axios'

export async function addPrivilegeToThingAccess(token: string,id:any, body : any): Promise<any> {
    return new Promise((resolve, reject) => {
        ///thing-accesses/{id}/privileges
    axios.post(API_URL + '/thingaccesses/'+id+'/privileges', body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                       
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}