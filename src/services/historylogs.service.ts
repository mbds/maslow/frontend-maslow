import { API_URL} from './env.service'
import axios from 'axios'

export async function getHistories(token: string): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/historiques', { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => { 
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}
