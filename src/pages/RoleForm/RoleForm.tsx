import { IonAlert, IonPage, IonHeader, IonContent, IonButton, IonItemDivider, IonLabel } from '@ionic/react';
import React, { useState, useEffect } from 'react';
import './RoleForm.css';
import { useHistory } from 'react-router';
import Button from '@material-ui/core/Button';
import { GetSessionToken, GetSessionUser } from '../../session'
import { useForm } from 'react-hook-form';
import { Toast } from '../../toast';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { getListRoles, addRole } from '../../services/roles.service';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles  } from '@material-ui/core/styles';
import Home from '../Home/Home';
import Administration from '../Administration/Administration';
import { LogoutUser } from '../../services/users.service';

let initialValues = {

};
const RoleForm: React.FC = () => {
  const token: any = GetSessionToken();
  const history = useHistory()
  const [lastId, setLastId] = useState('1');
  const [id, setID] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [, setbusy] = useState<boolean>(false)
  const [isSHown, setIsShown] = useState(false);  
  const user: any = GetSessionUser()
  // pour mettre async
  useEffect(() => {
    getRoles();
  }, []);

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin:theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591
  
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
    }),
  );
  const { register, handleSubmit } = useForm({
    defaultValues: initialValues
  });


  const classes = useStyles();

  const handleChangeID = (event: React.ChangeEvent<HTMLInputElement>) => {
    setID(event.target.value);
  };

  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };
  const handleChangeDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };
  async function getRoles() {
    let tmp = await getListRoles(token)
    if (tmp) {
      setLastId(tmp[tmp.length - 1].id);
      return lastId;
    }
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }
  
  function ListRole(){
    history.replace("/roles");
  }
  async function onSubmit() {
    let data = { id: '', name: '', description: '' };
    data.id = id;
    data.name = name;
    data.description = description;
    if (token) {
      if (1 == 1) {
        let tmp = await addRole(token, data);
        if (tmp) {
          Toast("Role ajouté avec succès");
          setIsShown(false);
          await sleep(100);
          history.replace('/roles');
        }
        else {
          Toast("Une erreur s'est produite lors de l'ajout du Role");
          setIsShown(true);
        }
      }

    }
  };

  function Roles() {
    history.replace('/roles')
  }

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  function Home() {
    history.replace('/dashboard')
  }

  function Administration() {
    history.replace('/administration')
  }
  return (
    <IonPage>      
      <IonHeader>
        <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb 
            label="Administration" 
            onClick={Administration}/>             
          <StyledBreadcrumb 
            label="Rôles" 
            onClick={Roles}/>   
          <StyledBreadcrumb 
            label="Ajouter Rôle" 
          />         
        </Breadcrumbs>
      </IonHeader>
      <IonContent >
      <IonItemDivider> <IonLabel> <h1>Formulaire rôle</h1> </IonLabel> </IonItemDivider>
      <form autoComplete="off" className="formDiv">
          <div>
          <span>Le dernier Id saisie : {lastId}</span><br></br>
            <TextField
              label="ID"
              style={{ margin: 8 }}
              placeholder="Nom"
              className="textField"
              id="outlined-margin-normal Nom"
              margin="normal"
              onChange={handleChangeID}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
          </div>
          <div>
            <TextField
              label="Nom"
              style={{ margin: 8 }}
              placeholder="ID"
              className="textField"
              id="outlined-margin-normal Nom"
              margin="normal"
              onChange={handleChangeName}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
          </div>
          <div>
            <TextField
              label="Description"
              style={{ margin: 8 }}
              placeholder="Description"
              className="textField"
              id="outlined-margin-normal Nom"
              margin="normal"
              onChange={handleChangeDescription}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
          </div>
          <IonButton className="bouton" color="secondary" onClick={onSubmit}>
            Ajouter
          </IonButton>
          <IonButton className="bouton" color="warning" type="reset">
            Annuler
           </IonButton>
          </form>
        <IonAlert
          isOpen={isSHown}
          onDidDismiss={() => setIsShown(false)}
          header={'Alert'}
          message={"Erreur lors de l'ajout du Role !"}
          buttons={['OK']}
        />
      </IonContent>
    </IonPage>
  );
};

export default RoleForm;
