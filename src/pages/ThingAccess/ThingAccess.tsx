import { useForm } from "react-hook-form";
import React, { useState, useEffect } from 'react';
import './ThingAccess.css';
import { useHistory, RouteComponentProps } from 'react-router';
import { GetSessionToken, GetSessionUser } from '../../session';
import { DetailUser, LogoutUser } from '../../services/users.service';
import { GetThings } from '../../services/things.service';
import { deleteThingAccess, addThingAccess } from "../../services/thingAccesses.service";
import { Toast } from "../../toast";
import { IonPage, IonHeader, IonContent, IonButton, IonItem, IonLabel, IonDatetime, IonItemDivider, IonToggle } from "@ionic/react";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown'
import { Checkbox, FormControlLabel } from "@material-ui/core";
import { detailPrivilege } from "../../services/privileges.service";


let periodicity = {

  start_date: "",
  end_date: "",
};


interface PageProps extends RouteComponentProps<{
  id: string;
}> { }


const ThingAccess: React.FC<PageProps> = ({ match }) => {


  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const id = match.params.id
  const history = useHistory()
  let [detailUser, setDetailUser] = useState<any>({ thingsAccesses: [] })
  const [, setbusy] = useState<boolean>(false)
  const [things, setthings] = useState<any>([])
  const [open, setOpen] = useState<any>([])
  const [show, setShow] = useState<any>([])
  const [thing, setThing] = useState<any>([])
  const [startDate, setStartDate] = useState<any>([])
  const [endDate, setEndDate] = useState<any>([])
  const [close, setClose] = useState<any>([])

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },

    }),
  );
  const classes = useStyles();
  // pour mettre async
  useEffect(() => {
    getUser();
    getthings();
  }, []);

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin: theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin: 0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

  const handleChangeThing = (event: React.ChangeEvent<HTMLInputElement>) => {
    setThing(event.target.value);
  };

  const handleChangeStart = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStartDate(event.target.value + "T10:30:06.709Z");
  };

  const handleChangeEnd = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEndDate(event.target.value + "T10:30:06.709Z");
  };

  const handleChangeOpen = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOpen(event.target.checked);
  };


  const handleChangeClose = (event: React.ChangeEvent<HTMLInputElement>) => {
    setClose(event.target.checked);
  };


  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  async function getthings() {
    setShow(false);
    let tmp: any[] = await GetThings(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      if(tmp[i].thingTypeUid === "lock"){
        setShow(true);
      }
      res.push({
        uid: tmp[i].UID,
        label: tmp[i].label,
        show: show
      })
    }
    setthings(res)

    setThing(res[0].uid);
  }

  function Administration() {
    history.replace('/administration')
  }

  function Home() {
    history.replace('/dashboard')
  }

  function Users() {
    history.replace('/users');
  }

  function DetUser() {
    history.replace('/userdetails/' + id);
  }
  async function getUser() {
    if (token) {
      let tmp = await DetailUser(token, id);
      setDetailUser(tmp);
    }
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  async function onSubmit() {
    let data = { thing_uid: '', userId: '', start_date: '', end_date: '',privileges:[{}]};
    data.userId = detailUser.id;
    data.start_date = startDate;
    data.end_date = endDate;
    data.thing_uid = thing;
      
    if (token) {
      if(open){
        let openPrivilege = await detailPrivilege(token, 1);
        data.privileges.push({id: openPrivilege.id});
      }
      if(close){
        let closePrivilege = await detailPrivilege(token, 2);
        data.privileges.push({id: closePrivilege.id});
      }
      if (addThingAccess(token, data)) {       
        await sleep(100);
        history.replace('/userdetails/' + id);
      }
    }
  };


  return (
    <IonPage>

      <IonHeader>
        <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration" onClick={Administration} />
          <StyledBreadcrumb label="Utilisateurs" onClick={Users} />
          <StyledBreadcrumb label="Details utilisateur" className="breadcrumb" onClick={DetUser} />
          <StyledBreadcrumb label="Ajout droit d'accès" />

        </Breadcrumbs>
      </IonHeader>
      <IonContent>
        <IonItemDivider> <IonLabel> <h1>Ajout nouveau droit d'accès</h1> </IonLabel> </IonItemDivider>
        <form className="formDiv">
          <div>
            <TextField
              className="textField"
              id="outlined-margin-normal roleId"
              select
              value={thing}
              onChange={handleChangeThing}
              label="Nom de l'objet"
              variant="outlined"
              required
              margin="normal"
              style={{ margin: 8 }}
            >
              {things.map((Element: any, i: number) => (
                <MenuItem key={i} value={Element.uid}>
                  {Element.label}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div>
            <TextField
              id="datetime-local"
              label="Début d'accès"
              type="date"
              style={{ margin: 8 }}
              defaultValue="2020-08-24"
              className="textField"
              variant="outlined"
              onChange={handleChangeStart}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
          <div>
            <TextField
              id="datetime-local"
              label="Fin d'accès"
              type="date"
              style={{ margin: 8 }}
              defaultValue="2020-08-24"
              className="textField"
              variant="outlined"
              onChange={handleChangeEnd}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
          
          {(show) ?
            <div>
            <FormControlLabel
            className="textField"
              control={
                <Checkbox
                  style={{ margin: 8 }}
                  checked={open}
                  onChange={handleChangeOpen}
                  color="primary"
                />
              }
              label="Ouvrir porte"
            />
          </div>
          :null}
          {(show) ?
          <div>
            <FormControlLabel
            className="textField"
            control={
                <Checkbox
                  style={{ margin: 8 }}
                  checked={close}
                  onChange={handleChangeClose}
                  color="primary"
                />
              }
              label="Fermer porte"
            />
          </div>
           :null}
          <IonButton className="bouton" color="secondary" onClick={onSubmit}>
            Ajouter nouvel accès
          </IonButton>
        </form>
      </IonContent>
    </IonPage >
  );
};
export default ThingAccess;
