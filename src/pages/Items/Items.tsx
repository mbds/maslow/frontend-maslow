import {
  IonContent, IonHeader, IonPage, IonList, IonItem, IonButton,
  IonLabel, IonToggle, IonRange, IonIcon, IonItemGroup, IonItemDivider} from '@ionic/react';

import React, { useState, useEffect } from 'react';
import './Items.css';
import { useHistory, RouteComponentProps } from 'react-router';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { Theme, emphasize, withStyles  } from '@material-ui/core/styles';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown'
import { thermometer, contrast } from 'ionicons/icons'

import { GetSessionToken, GetSessionUser } from '../../session'
import { LogoutUser } from '../../services/users.service'
import { GetThing, GetItem, UpdateItem } from '../../services/things.service';

interface PageProps extends RouteComponentProps<{
  thingUid: string;
}> { }


const Items: React.FC<PageProps> = ({ match }) => {

  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const [items, setitems] = useState<any>([])
  const uid = match.params.thingUid
  const [, setthingname] = useState<any>([])

  // pour mettre async
  useEffect(() => {
    getitems();
  }, []);

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin:theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591
  
  async function getitems() {
    let thing: any = await GetThing(token, uid)
    let channels: any[] = thing.channels
    let tmp: any = []

    setthingname(thing.label)
    if (channels && channels.length > 0) {
      for (let i = 0; i < channels.length; i++) {
        if(channels[i].linkedItems.length > 0){
          let item = channels[i].linkedItems[0]
          let defaultTags = channels[i].defaultTags
          let itemsInfos = await GetItem(token, item)
          tmp.push({
            name: item,
            defaultTags: defaultTags,
            itemInfos: itemsInfos
          })
        }
        
      }
    }
    setitems(tmp)
  }


  async function updateitems(name: string, value: string) {
    UpdateItem(token, name, uid, value)
    await sleep(3000)    // probleme de temps de reponse apres le changement de state d'un objet.
    getitems()
  }

  const history = useHistory()
  const [, setbusy] = useState<boolean>(false)

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  function Administration() {
    history.replace('/administration');
  }

  function Home() {
    history.replace('/dashboard');
  }
  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }


  return (
    <IonPage>
      <IonHeader>
      <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link active">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
            className="breadcrumb"
          />
          <StyledBreadcrumb label="Item" className="breadcrumb" />          
        </Breadcrumbs>
      </IonHeader>
      <IonContent >
        <IonList>

          {items.map((Element: any) => (

            <IonItemGroup key={Element.name}>

              <IonItemDivider> <IonLabel> <h1>{Element.itemInfos.label}</h1> </IonLabel> </IonItemDivider>

              {(Element.defaultTags.length !== 0) ?

                Element.defaultTags.map((i: any) => (
                  (i === "Lighting") ?
                    <div>
                      <IonItem key={Element.name}>
                        <IonRange min={0} max={100} value={Element.itemInfos.state} step={1} ticks={false} snaps={true} pin color="secondary"
                          onIonChange={(e: any) => updateitems(Element.name, e.target.value)} >
                          <IonIcon slot="start" size="small" icon={contrast}></IonIcon>
                          <IonIcon slot="end" icon={contrast}></IonIcon>
                        </IonRange>
                      </IonItem>

                      <IonItem>
                        <IonLabel> ON/OFF </IonLabel>
                        <IonToggle checked={(Element.itemInfos.state !== 0)} onIonChange={(e: any) => updateitems(Element.name, (e.detail.checked ? '100' : '0'))} />
                      </IonItem>

                    </div>
                    : null

                ))

                : null
              }

              {(Element.defaultTags.length === 0 && Element.itemInfos.type === 'Dimmer') ?
                <IonItem key={Element.name}>
                  <IonRange min={0} max={100} step={1} value={Element.itemInfos.state} ticks={false} snaps={true} pin color="secondary"
                    onIonChange={(e: any) => updateitems(Element.name, e.target.value)}>
                    <IonIcon slot="start" size="small" color="danger" icon={thermometer}></IonIcon>
                    <IonIcon slot="end" color="danger" icon={thermometer}></IonIcon>
                  </IonRange>
                </IonItem>
                : null
              }

              {(Element.defaultTags.length === 0 && Element.itemInfos.type === 'Number') ?
                <IonItem key={Element.name}>
                  <IonLabel className="centrer" >{Element.itemInfos.state}</IonLabel>
                </IonItem>
                : null
              }

            {(Element.defaultTags.length === 0 && Element.itemInfos.type === 'Switch') ?
              <IonItem key={Element.name}>
                <IonLabel className="centrer">{Element.itemInfos.state}</IonLabel>
                <IonToggle checked={(Element.itemInfos.state !== "OFF")} onIonChange={(e: any) => updateitems(Element.name, (e.detail.checked ? 'ON' : 'OFF'))} />
              </IonItem>
                : null
              }
            </IonItemGroup>

          ))}
        </IonList>


      </IonContent>
    </IonPage>
  );
};

export default Items;
