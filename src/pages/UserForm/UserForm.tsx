import {
  IonContent, IonHeader, IonPage, IonButton, IonAlert, IonItemDivider, IonLabel
} from '@ionic/react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import React, { useState, useEffect } from 'react';
import './UserForm.css';
import { useHistory } from 'react-router';
import { GetSessionToken, GetSessionUser } from '../../session'
import { LogoutUser, addUser, ListUsers } from '../../services/users.service'
import { useForm } from 'react-hook-form';
import { Toast } from '../../toast';
import { getListRoles } from '../../services/roles.service';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles  } from '@material-ui/core/styles';

const UserForm: React.FC = () => {
  const user: any = GetSessionUser();
  const token: any = GetSessionToken();
  const [roles, setroles] = useState<any>([]);
  const history = useHistory()
  const [busy, setbusy] = useState<boolean>(false);
  const [lastId, setLastId] = useState('1');
  const [isSHown, setIsShown] = useState(false);

  const [lastname, setLastname] = useState(''); // '' initialise
  const [firstname, setFirstname] = useState('');
  const [email, setEmail] = useState(''); // '' initialise
  const [password, setPassword] = useState('');
  const [role, setRole]= useState('');
  // pour mettre async
  useEffect(() => {
    getroles();
    getListUsers();
  }, []);
  
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
    }),
  );

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin:theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591
  
  const classes = useStyles();

  const handleChangeRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRole(event.target.value);
  };

  const handleChangeFirstname = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFirstname(event.target.value);
  };
  const handleChangeLastname = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLastname(event.target.value);
  };
  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };
  const handleChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  function Home() {
    history.replace('/dashboard')
  }

  function Administration() {
    history.replace('/administration')
  }

  function Users() {
    history.replace('/users');
  }

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  async function getListUsers() {
    let tmp = await ListUsers(token)
    if (tmp) {
      setLastId(tmp[tmp.length - 1].id);
      return lastId;
    }

  }

  async function getroles() {
    let tmp: any[] = await getListRoles(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        id: tmp[i].id,
        description: tmp[i].description
      })
    }
    setroles(res);
    setRole(res[0].id);
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  async function submit() {
    let data = {lastname:'', firstname:'', email:'', password:'', roleId:''};
    data.lastname=lastname;
    data.firstname = firstname;
    data.email= email;
    data.password = password;
    data.roleId = role;
    if (token) {
        let tmp = await addUser(token, data);
        if (tmp) {
          await sleep(1000);
          history.replace('/users');
        }
        else {
          setIsShown(true);
        }
    }
  };

  return (
    <IonPage>
      <IonHeader>
      <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration" onClick={Administration} />
          <StyledBreadcrumb label="Utilisateurs" onClick={Users} />
          <StyledBreadcrumb label="Ajouter utilisateur"/>          
        </Breadcrumbs>
      </IonHeader>  
      <IonContent >
      <IonItemDivider> 
        <IonLabel> 
          <h1>Formulaire utilisateur</h1> 
        </IonLabel> 
      </IonItemDivider>
        <form autoComplete="off" className="formDiv">
          <div>
          <TextField              
              label="Nom"
              style={{ margin: 8 }}
              placeholder="Nom"
              className="textField"
              id="outlined-margin-normal Nom"
              margin="normal"
              onChange={handleChangeLastname}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
            </div>
            <div>
             <TextField
              label="Prénom"
              style={{ margin: 8 }}
              placeholder="Prénom"
              onChange={handleChangeFirstname}
              className="textField"
              id="outlined-margin-normal prenom"
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
            </div>
            <div>
            <TextField
              label="Email"
              style={{ margin: 8 }}
              placeholder="Email"
              className="textField"
              id="outlined-margin-normal email"
              margin="normal"
              onChange={handleChangeEmail}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
            </div>
            <div>
             <TextField
              className="textField"
              id="outlined-margin-normal password"
              label="Password"
              type="password"
              style={{ margin: 8 }}
              placeholder="Password"
              onChange={handleChangePassword}
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              required
            />
            </div>
            <div>
            <TextField
              className="textField"
              id="outlined-margin-normal roleId"
              select
              value={role}
              style={{ margin: 8 }}
              onChange={handleChangeRole}
              label="Role"
              margin="normal"
              variant="outlined"
              required
              InputLabelProps={{
                shrink: true,
              }}
            >
              {roles.map((Element:any) => (
                <MenuItem  className="menuItem" key={Element.id} value={Element.id}>
                  {Element.name}{Element.description}
                </MenuItem>
              ))}
            </TextField>
            </div>
            <IonButton className="bouton" color="secondary" onClick={submit}>
              Ajouter
            </IonButton>
            <IonButton className="bouton" color="warning" type="reset">
              Annuler
            </IonButton>
            
        </form>
        <IonAlert
          isOpen={isSHown}
          onDidDismiss={() => setIsShown(false)}
          header={'Alert'}
          message={"Erreur lors de l'ajout de l'utilisateur !"}
          buttons={['OK']}
        />
      </IonContent>
    </IonPage>
  );
};

export default UserForm;
