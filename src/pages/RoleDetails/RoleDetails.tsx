import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem,
  IonAvatar, IonLabel, IonItemSliding, IonItemOption, IonItemOptions, IonButton, IonInput, IonLoading, IonRow, IonGrid, IonCol, IonButtons, IonSelect, IonDatetime, IonSelectOption, IonText, IonAlert, IonItemDivider
} from '@ionic/react';
import { Toast } from '../../toast'
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useForm, Controller } from "react-hook-form";
import React, { useState, useRef, useEffect } from 'react';
import './RoleDetails.css';
import { useHistory, RouteComponentProps } from 'react-router';
import { addThingAccess, deleteThingAccess } from '../../services/thingAccesses.service';
import { GetSessionToken, GetSessionUser } from '../../session';
import { GetThings } from '../../services/things.service';
import { getListRoles, deleteRole, detailRole, updateRole } from '../../services/roles.service';
import { LogoutUser } from '../../services/users.service';
import RoleForm from '../RoleForm/RoleForm';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles } from '@material-ui/core/styles';
import Home from '../Home/Home';
import Roles from '../Roles/Roles';
let initialValues = {
  thing_uid: "",
  start_date: "",
  end_date: "",
};


interface PageProps extends RouteComponentProps<{
  id: string;
}> { }


const RoleDetails: React.FC<PageProps> = ({ match }) => {


  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const id = match.params.id
  const history = useHistory()
  let [role, setRole] = useState({
    id: '',
    name: '',
    description: ''
  });
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [isSHown, setIsShown] = useState(false);
  const [busy, setbusy] = useState<boolean>(false);
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [showDeleteNotOK, setShowDeleteNotOK] = useState(false);
  const { register, handleSubmit, errors, formState } = useForm({
    defaultValues: initialValues
  });

  // pour mettre async
  useEffect(() => {
    getRole();
  }, []);


  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
    }),
  );

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin: theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

  const classes = useStyles();
  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };
  const handleChangeDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };
  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }
  function Roles() {
    history.replace('/roles')
  }
  function Administration() {
    history.replace('/administration')
  }

  function Home() {
    history.replace('/dashboard')
  }
  function roles() {
    history.replace('/roles')
  }
  async function getRole() {
    if (token) {
      let tmp = await detailRole(token, id);
      await sleep(100);
      setRole({ id: tmp.id, name: tmp.id, description: tmp.description });
      setName(tmp.name);
      setDescription(tmp.description);
      if (tmp) {
        setIsShown(true);
      }
      else {
        setIsShown(false);
      }

    }
  }

  async function Supprimer(id: string) {
    if (deleteRole(token, id)) {
      await sleep(100);
      history.replace('/roles');
    }
    else {
      setShowDeleteNotOK(true);
    }

  }

  const onSubmit = () => {
    if (token) {
      if (updateRole(token, role.id, name, description)) {
        Toast("Role modifié avec succès")
        history.replace('/roles');
      }
    }
  };


  return (
    <IonPage><IonHeader>
      <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Item>
              <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
            </Nav.Item>
            {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
            {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
        <StyledBreadcrumb
          label="Home"
          icon={<HomeIcon fontSize="small" />}
          onClick={Home}
        />
        <StyledBreadcrumb
          label="Administration"
          onClick={Administration} />
        <StyledBreadcrumb
          label="Rôles"
          onClick={Roles} />
        <StyledBreadcrumb
          label="Détails Rôle"
        />
      </Breadcrumbs>
    </IonHeader>
      <IonContent>
      <IonItemDivider> <IonLabel> <h1>Détails rôle</h1> </IonLabel> </IonItemDivider>
        {(role !== null) ?
          <form className="formDiv">
            <div>
              <div>
                <TextField
                  label="Nom"
                  style={{ margin: 8 }}
                  placeholder="Nom"
                  value={name}
                  onChange={handleChangeName}
                  className="textField"
                  id="outlined-margin-normal nom"
                  margin="normal"
                  variant="outlined"
                  required
                />
              </div>
              <div>
                <TextField
                  label="Description"
                  style={{ margin: 8 }}
                  placeholder="Description"
                  className="textField"
                  id="outlined-margin-normal email"
                  margin="normal"
                  value={description}
                  onChange={handleChangeDescription}
                  variant="outlined"
                  required
                />
              </div>
              <IonButton className="bouton" color="secondary" onClick={onSubmit}>
                Enregistrer
              </IonButton>
              <IonButton className="bouton" color="secondary">
                <a onClick={() => setShowDeleteAlert(true)}>Supprimer</a>
              <IonAlert
                  isOpen={showDeleteAlert}
                  onDidDismiss={() => setShowDeleteAlert(false)}
                  header={'Êtes-vous sûr ?'}
                  message={'Vous allez supprimer ce rôle définitivement !'}
                  buttons={[
                    {
                      text: 'Supprimer',
                      handler: () => {
                        Supprimer(role.id);
                      }
                    },
                    {
                      text: 'Annuler',
                      handler: () => {
                      }
                    }
                  ]}

                />
              </IonButton>

              <IonAlert
                isOpen={showDeleteNotOK}
                onDidDismiss={() => setShowDeleteNotOK(false)}
                header={'Alert'}
                message={"Une erreur s'est produite lors de la supression du rôle !"}
                buttons={['OK']}
              />
            </div>
          </form> : null}
      </IonContent>
    </IonPage>
  );
};

export default RoleDetails;
