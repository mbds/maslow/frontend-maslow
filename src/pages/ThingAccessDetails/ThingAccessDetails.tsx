import React, { useState, useEffect } from 'react';
import './ThingAccessDetails.css';
import { useHistory, RouteComponentProps } from 'react-router';
import { GetSessionToken, GetSessionUser } from '../../session';
import { LogoutUser } from '../../services/users.service';
import { getThingAccess, updateThingAccess, deleteThingAccess } from "../../services/thingAccesses.service";
import { IonPage, IonHeader, IonContent, IonButton,IonItem, IonLabel, IonDatetime, IonItemDivider, IonAlert } from "@ionic/react";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown'
import { Checkbox } from "@material-ui/core";
import { GetThings } from '../../services/things.service';
import { Toast } from '../../toast';



interface PageProps extends RouteComponentProps<{
  id: string;
}> { }


const ThingAccessDetails: React.FC<PageProps> = ({ match }) => {


  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const id = match.params.id
  const history = useHistory()
  const [things, setThings] = useState<any>([])
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [thing, setThing] = useState<any>([])
  const [startDate, setStartDate] = useState<any>([])
  const [endDate, setEndDate] = useState<any>([])
  const [periodic, setPeriodic] = useState<boolean>(false)
  const [, setbusy] = useState<boolean>(false);
  const useStyles = makeStyles(() =>
    createStyles({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },

    }),
  );
  // pour mettre async
  useEffect(() => {
    ThingAccess();
    getthings();
  }, []);

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin: theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin: 0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

  const handleChangeThing = (event: React.ChangeEvent<HTMLInputElement>) => {
    setThing(event.target.value);
  };
  
  const handleChangeStart = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStartDate(event.target.value);
  };

  const handleChangeEnd = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEndDate(event.target.value);
  };
  async function getthings() {
    let tmp: any[] = await GetThings(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        uid: tmp[i].UID,
        label: tmp[i].label
      })
    }
    setThings(res)
  }

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  function Administration() {
    history.replace('/administration')
  }

  function Home() {
    history.replace('/dashboard')
  }

  function Users() {
    history.replace('/users');
  }

  async function ThingAccess() {
    if (token) {
      let tmp = await getThingAccess(token, id);
      setThing(tmp.thing_uid);
      let startDate = new Date(tmp.start_date).toISOString().slice(0,10);
      let edDate = new Date(tmp.end_date).toISOString().slice(0,10);
      setStartDate(startDate);
      //Date.parse(startDate).toString("YYYY-MM-DD") 
      console.log("date startdate:", startDate);
      setEndDate(edDate);
    }
  }

  function DetUser() {
    history.replace('/userdetails/' + id);
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  async function SupprimerAccess() {
    if (deleteThingAccess(token, id)) {
      Toast("Suppression Réussite")
      await sleep(100);
      DetUser();
    }
  }

  async function onSubmit() {
    let data = { thing_uid: '', start_date: '', end_date: '', periodic: false };
    data.start_date = startDate+"T10:30:06.709Z";
    data.end_date = endDate+"T10:30:06.709Z";
    data.thing_uid = thing;
    data.periodic = periodic;
    if (token) {
      if (updateThingAccess(token, id, data)) {
        await sleep(100);
        history.replace('/userdetails/' + id);
      }
    }
  };


  return (
    <IonPage>
      <IonHeader>
        <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration" onClick={Administration} />
          <StyledBreadcrumb label="Utilisateurs" onClick={Users} />
          <StyledBreadcrumb label="Details utilisateur" className="breadcrumb" onClick={DetUser} />
          <StyledBreadcrumb label="Ajout droit d'accès" />
        </Breadcrumbs>
      </IonHeader>
      <IonContent>
        <IonItemDivider> <IonLabel> <h1>Modifier droit d'accès</h1> </IonLabel> </IonItemDivider>
        <form className="formDiv">
          <div>
            <TextField
              className="textField"
              id="outlined-margin-normal roleId"
              select

              value={thing}
              onChange={handleChangeThing}
              label="Nom de l'objet"
              variant="outlined"
              required
              disabled
              margin="normal"
              style={{ margin: 8 }}
            >
              {things.map((Element: any, i: number) => (
                <MenuItem key={i} value={Element.uid}>
                  {Element.label}<br></br>
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div>
            <TextField
              id="datetime-local"
              label="Début d'accès"
              type="date"
              value={startDate}
              className="textField"            
              onChange={handleChangeStart}
              style={{ margin: 8 }}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
          <div>
            <TextField
              id="datetime-local"
              label="Fin d'accès"
              style={{ margin: 8 }}
              type="date"
              value={endDate}
              className="textField"
              onChange={handleChangeEnd}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
          <IonButton className="bouton" color="secondary" onClick={onSubmit}>Enregistrer</IonButton>
          <IonButton className="bouton" color="danger"><a onClick={() => setShowDeleteAlert(true)}>Supprimer</a>
          </IonButton>
            <IonAlert
              isOpen={showDeleteAlert}
              onDidDismiss={() => setShowDeleteAlert(false)}
              header={'Êtes-vous sûr ?'}
              message={"Vous allez supprimer ce droit d'accès définitivement !"}
              buttons={[
                {
                  text: 'Supprimer',
                  handler: () => {
                    SupprimerAccess();
                  }
                },
                {
                  text: 'Annuler',
                  handler: () => {
                  }
                }
              ]}
            />
        </form>
      </IonContent>
    </IonPage >
  );
};
export default ThingAccessDetails;
