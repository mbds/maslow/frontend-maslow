import {
  IonContent, IonHeader, IonPage, IonButton, IonAlert, IonItemDivider, IonLabel
} from '@ionic/react';
import { Toast } from '../../toast'
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown'

import { useForm } from "react-hook-form";
import './UserDetails.css';
import { useHistory, RouteComponentProps } from 'react-router';
import { deleteThingAccess } from '../../services/thingAccesses.service';
import { GetSessionToken, GetSessionUser } from '../../session';
import { GetThings, GetThing } from '../../services/things.service';
import { DetailUser, deleteUser, updateUser, LogoutUser } from '../../services/users.service';
import { getListRoles } from '../../services/roles.service';
import React, { useState, useEffect } from 'react';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';

import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles } from '@material-ui/core/styles';

interface PageProps extends RouteComponentProps<{
  id: string;
}> { }


const UserDetails: React.FC<PageProps> = ({ match }) => {

  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const id = match.params.id
  const history = useHistory()
  let [detailUser, setDetailUser] = useState<any>({ thingsAccesses: [] })
  const [roles, setroles] = useState<any>([])
  const [isSHown, setIsShown] = useState(false);
  const [, setthings] = useState<any>([])
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [showDeleteNotOK, setShowDeleteNotOK] = useState(false);
  const [, setbusy] = useState<boolean>(false);
  const [lastname, setLastname] = useState(''); // '' initialise
  const [firstname, setFirstname] = useState('');
  const [email, setEmail] = useState(''); // '' initialise
  const [role, setRole] = useState('');
  const [thingAccesses, setThingAccess] = useState([]);
  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin: theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin: 0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

  const handleChangeRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRole(event.target.value);
  };

  const handleChangeFirstname = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFirstname(event.target.value);
  };
  const handleChangeLastname = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLastname(event.target.value);
  };
  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  // pour mettre async
  useEffect(() => {
    getUser();
    getthings();
    getroles();;
  }, []);

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
    }),
  );
  const classes = useStyles();
  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  async function getroles() {
    let tmp: any[] = await getListRoles(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        id: tmp[i].id,
        description: tmp[i].description
      })
    }
    setroles(res)
  }
  async function SupprimerAccess(id: string) {
    if (deleteThingAccess(token, id)) {
      Toast("Suppression Réussite")
      await sleep(100);
      getUser();
    }
  }

  function ThingAccess() {
    history.replace('/accesses/' + id)
  }

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  async function getthings() {
    let tmp: any[] = await GetThings(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        uid: tmp[i].UID,
        label: tmp[i].label
      })
    }
    setthings(res)
  }

  async function getUser() {
    let aux: any = [];
    if (token) {
      let tmp = await DetailUser(token, id);
      if (tmp) {
        setIsShown(true);
      }
      else {
        setIsShown(false);
      }
      setDetailUser(tmp);
      setRole(tmp.roleId);
      setFirstname(tmp.firstname);
      setEmail(tmp.email);
      setLastname(tmp.lastname);
      for (let i = 0; i < tmp.thingsAccesses.length; i++) {
        let thing = await GetThing(token, tmp.thingsAccesses[i].thing_uid);
        let start_date = new Date(tmp.thingsAccesses[i].start_date);
        let end_date = new Date(tmp.thingsAccesses[i].end_date);
        var start = ("00" + start_date.getDate()).slice(-2) + "/" +
          ("00" + (start_date.getMonth() + 1)).slice(-2) + "/" +
          start_date.getFullYear() + " à " +
          ("00" + start_date.getHours()).slice(-2) + ":" +
          ("00" + start_date.getMinutes()).slice(-2) + ":" +
          ("00" + start_date.getSeconds()).slice(-2);
        var end = ("00" + end_date.getDate()).slice(-2) + "/" +
          ("00" + (end_date.getMonth() + 1)).slice(-2) + "/" +
          end_date.getFullYear() + " à " +
          ("00" + end_date.getHours()).slice(-2) + ":" +
          ("00" + end_date.getMinutes()).slice(-2) + ":" +
          ("00" + end_date.getSeconds()).slice(-2);
        aux.push({
          id: tmp.thingsAccesses[i].id,
          thingLabel: thing.label,
          start_date: start,
          end_date: end,
        });
      }
      setThingAccess(aux);
      getthings();

    }
  }

  async function Supprimer(id: any) {
    if (deleteUser(token, id)) {
      await sleep(100);
      history.replace('/users');
    }
    else {
      setShowDeleteNotOK(true);
    }

  }

  /* function Bloquer(id : string ) {
    Toast("bloquer");
  }
   */

  function Users() {
    history.replace('/users');
  }

  function Administration() {
    history.replace('/administration');
  }

  function Home() {
    history.replace('/dashboard');
  }

  function Detail(id: string) {
    history.replace('/thingaccess/' + id)
  }

  async function onSubmit() {
    let data = { lastname: '', firstname: '', email: '', roleId: '' };
    data.lastname = lastname;
    data.firstname = firstname;
    data.email = email;
    data.roleId = role;
    if (token) {
      if (updateUser(token, detailUser.id, data)) {
        await sleep(100);
        history.replace('/users');
      }
    }
  };


  return (
    <IonPage>
      <IonHeader>
        <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration" onClick={Administration} />
          <StyledBreadcrumb label="Utilisateurs" onClick={Users} />
          <StyledBreadcrumb label="Details utilisateur" />
        </Breadcrumbs>
      </IonHeader>
      <IonContent className="content userDetails">
        <IonItemDivider> <IonLabel> <h1>Détails utilisateur</h1> </IonLabel> </IonItemDivider>
        {(detailUser !== null) ?
          <form className="formDiv">
            <div>
              <div>
                <TextField
                  label="Nom"
                  style={{ margin: 8 }}
                  placeholder="Nom"
                  value={lastname}
                  onChange={handleChangeLastname}
                  className="textField"
                  id="outlined-margin-normal Nom"
                  margin="normal"
                  variant="outlined"
                  required
                />
              </div>
              <div>
                <TextField
                  label="Prénom"
                  style={{ margin: 8 }}
                  placeholder="Prénom"
                  value={firstname}
                  onChange={handleChangeFirstname}
                  className="textField"
                  id="outlined-margin-normal prenom"
                  margin="normal"
                  variant="outlined"
                  required
                />
              </div>
              <div>
                <TextField
                  label="Email"
                  style={{ margin: 8 }}
                  placeholder="Email"
                  className="textField"
                  id="outlined-margin-normal email"
                  margin="normal"
                  value={email}
                  onChange={handleChangeEmail}
                  variant="outlined"
                  required
                />
              </div>
              <div>
                <TextField
                  className="textField"
                  id="outlined-margin-normal roleId"
                  select
                  margin="normal"
                  style={{ margin: 8 }}
                  value={role}
                  onChange={handleChangeRole}
                  label="Role"
                  variant="outlined"
                  required
                >
                  {roles.map((Element: any, i: number) => (
                    <MenuItem key={i} value={Element.id}>
                      {Element.name} {Element.description}
                    </MenuItem>
                  ))}
                </TextField>
              </div>
              <IonButton color="secondary" onClick={onSubmit}>Enregistrer</IonButton>
              <IonButton color="danger"><a onClick={() => setShowDeleteAlert(true)}>Supprimer</a>
                <IonAlert
                  isOpen={showDeleteAlert}
                  onDidDismiss={() => setShowDeleteAlert(false)}
                  header={'Êtes-vous sûr ?'}
                  message={'Vous allez supprimer cet utilisateur définitivement !'}
                  buttons={[
                    {
                      text: 'Supprimer',
                      handler: () => {
                        Supprimer(detailUser.id);
                      }
                    },
                    {
                      text: 'Annuler',
                      handler: () => {
                      }
                    }
                  ]}

                />
              </IonButton>

              <IonAlert
                isOpen={showDeleteNotOK}
                onDidDismiss={() => setShowDeleteNotOK(false)}
                header={'Alert'}
                message={"Une erreur s'est produite lors de la supression de l'utilisateur !"}
                buttons={['OK']}
              />
            </div>
          </form> : null}
        <pre></pre>
        <IonItemDivider> <IonLabel> <h1>Liste des objets connectées</h1> </IonLabel> </IonItemDivider>
        {
          (!isSHown) ?
            <p className="para">
              Aucune donnée
          </p>
            : null
        }
        {
          (isSHown) ?
            <TableContainer component={Paper} className="container-fluid">
              <Table className="table" size="small" aria-label="a dense table">
                <TableHead >
                  <TableRow className="d-flex row">
                    <TableCell className="col-lg-3">Objet</TableCell>
                    <TableCell className="col-lg-3">Début d'accès</TableCell>
                    <TableCell className="col-lg-3">Fin d'accès</TableCell>
                    <TableCell className="col-lg-3">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {thingAccesses.map((element: any) => (
                    <TableRow key={element.id} className="d-flex row">
                      <TableCell className="col-lg-3">{element.thingLabel}</TableCell>
                      <TableCell className="col-lg-3">{element.start_date}</TableCell>
                      <TableCell className="col-lg-3">{element.end_date}</TableCell>
                      <TableCell className="col-lg-3">
                        <a onClick={() => Detail(element.id)}   >
                          <img className="image" src="assets/images/modifier.png" title="Modifier un droit aux objets" />
                        </a>
                        <a onClick={() => setShowDeleteAlert(true)}   >
                          <img className="image" src="assets/images/supprimer.png" title="Supprimer un droit aux objets" />
                        </a>
                      </TableCell>
                      <IonAlert
                        isOpen={showDeleteAlert}
                        onDidDismiss={() => setShowDeleteAlert(false)}
                        header={'Êtes-vous sûr ?'}
                        message={"Vous allez supprimer ce droit d'accès définitivement !"}
                        buttons={[
                          {
                            text: 'Supprimer',
                            handler: () => {
                              SupprimerAccess(element.id);
                            }
                          },
                          {
                            text: 'Annuler',
                            handler: () => {
                            }
                          }
                        ]}

                      />
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            : null}

        <IonButton color="secondary" type="submit" onClick={ThingAccess}>Ajouter droit d'accès aux objets</IonButton>
      </IonContent >
    </IonPage >
  );
};

export default UserDetails;
