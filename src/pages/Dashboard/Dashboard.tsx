import {
  IonHeader, IonPage, IonButton, IonLoading, IonContent, IonList, IonItemGroup, IonItemDivider, IonLabel, IonItemSliding, IonItem, IonAvatar, IonIcon, IonItemOptions, IonItemOption
} from '@ionic/react';
import React, { useState, useEffect } from 'react';
import './Dashboard.css';
import { useHistory } from 'react-router';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import Typography from '@material-ui/core/Typography';
import NavDropdown from 'react-bootstrap/NavDropdown'
import { GetSessionToken, GetSessionUser } from '../../session'
import { LogoutUser } from '../../services/users.service'
import { lock, unlock } from 'ionicons/icons';
import { person } from 'ionicons/icons';
import Button from '@material-ui/core/Button';
import { GetLocks, GetLock, OpenLock, CloseLock } from '../../services/lock.service'
import { GetThings } from '../../services/things.service';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { Theme, emphasize, withStyles } from '@material-ui/core/styles';



const Dashboard: React.FC = () => {
  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const [locks, setlocks] = useState<any>([]);
  const [things, setthings] = useState<any>([]);
  const [] = useState(false);
  const [busy, setbusy] = useState<boolean>(false)

  const history = useHistory();
  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin: theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

  // pour mettre async
  useEffect(() => {
    getlocks();
    getthings();
  }, []);

  async function getlocks() {
    let tmp: any[] = await GetLocks(token)
    for (let i = 0; i < tmp.length; i++) {
      tmp[i].infos = await GetLock(token, tmp[i].id)
    }
    setlocks(tmp)
  }

  async function getthings() {
    let tmp: any[] = await GetThings(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        uid: tmp[i].UID,
        label: tmp[i].label
      })
    }
    setthings(res)
  }


  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  function Administration() {
    history.replace('/administration')
  }

  function Home() {
    history.replace('/dashboard')
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }


  async function openLock(id: string) {
    await OpenLock(token, id)
    await sleep(10000) // probleme de serrure qui prends du temps pour repondre
    getlocks()
  }

  async function closeLock(id: string) {
    await CloseLock(token, id)
    await sleep(10000)
    getlocks()

  } 

  function AccesItems(thingUid: any) {
    history.replace('/items/' + thingUid)
  }

  return (
    <IonPage className="home">
      <IonHeader>
        <Navbar bg="secondary" expand="lg" className="">
          <Navbar.Toggle />
          <Navbar.Collapse>
            <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link active">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link">Administration</Nav.Link>
              </Nav.Item>
              :null}            

              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs  className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
        </Breadcrumbs>
      </IonHeader>
      <IonContent>        
        <IonList>
          <IonItemGroup>
            <IonItemDivider> 
              <Typography variant="h5" className="h5" gutterBottom>
                Listes des serrures
              </Typography>
            </IonItemDivider>
            {locks.map((Element: any) => (
              <IonItemSliding key={Element.id}>
                <IonItem >
                  <IonAvatar>
                    <img src="https://cdn0.iconfinder.com/data/icons/internet-of-things-iot-1/68/Door_door_lock_lock_network_remote_access_wifi_icon-512.png" />
                  </IonAvatar>
                  {(Element.infos.etat === 'close') ?
                    <IonIcon icon={lock} size="large" slot="end">  </IonIcon> :
                    <IonIcon icon={unlock} size="large" slot="end">  </IonIcon>
                  }

                  <IonLabel className="ion-padding" >
                    <h2>{Element.infos.nom}
                    </h2>
                  </IonLabel>

                </IonItem>

                {(Element.infos.etat === 'close') ?
                  <IonItemOptions side="end">
                    <IonItemOption onClick={() => openLock(Element.id)} color="primary">OPEN</IonItemOption>
                  </IonItemOptions>
                  :
                  <IonItemOptions side="end">
                    <IonItemOption onClick={() => closeLock(Element.id)} color="primary">CLOSE</IonItemOption>
                  </IonItemOptions>
                }
              </IonItemSliding>
            ))}
          </IonItemGroup>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <IonItemGroup>
            <IonItemDivider> 
              <Typography variant="h5" className="h5" gutterBottom>
                Liste des objets connectés (Hors serrures)
              </Typography>
              </IonItemDivider>
            {things.map((Element: any) => (
              <IonItemSliding key={Element.uid}>
                <IonItem >
                  <IonAvatar>
                    <img src="https://www.digitalunite.com/sites/default/files/images/shutterstock_140577157.jpg" />
                  </IonAvatar>

                  <IonLabel className="ion-padding" >
                    <h2>{Element.label}</h2>
                  </IonLabel>

                </IonItem>

                <IonItemOptions side="end">
                  <IonItemOption onClick={() => AccesItems(Element.uid)} color="primary">Options</IonItemOption>
                </IonItemOptions>

              </IonItemSliding>
            ))}
          </IonItemGroup>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Dashboard;
