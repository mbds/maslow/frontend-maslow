import {
  IonContent, IonHeader, IonPage, IonButton, IonItemDivider, IonLabel} from '@ionic/react';


import Button from '@material-ui/core/Button';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import React, { useState, useEffect } from 'react';
import './HistoryLogs.css';
import { useHistory } from 'react-router';
import { GetSessionToken, GetSessionUser } from '../../session';
import { LogoutUser, DetailUser } from '../../services/users.service';

import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import {Theme, emphasize, withStyles  } from '@material-ui/core/styles';
import { getHistories } from '../../services/historylogs.service';
import { GetThing, GetItem } from '../../services/things.service';
import { TablePagination } from '@material-ui/core';


const HistoryLogs: React.FC = () => {

  const token: any = GetSessionToken()
  const user: any = GetSessionUser()
  const [historiques, setHistoriques] = useState<any>([])
  const [isSHown, setIsShown] = useState(false);
  const history = useHistory();
  const [busy, setbusy] = useState<boolean>(false)


  // pour mettre async
  useEffect(() => {
    getHistoryLogs();
  }, []);

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin:theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591
  
  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  async function getHistoryLogs() {
    let histories:any = [];
    if (token) {
      let tmp = await getHistories(token);
      if (tmp.length <= 0) {
        setIsShown(false);
      }
      else {
        for (let i =0; i< tmp.length; i++){
          let user = await DetailUser(token, tmp[i].userId);
          let thing = await GetThing(token, tmp[i].thingId);
          let item = await GetItem(token, tmp[i].itemName);
          let creationDate = new Date( tmp[i].creationDate);
          var creation = ("00" + creationDate.getDate()).slice(-2)  + "/" +
          ("00" +(creationDate.getMonth() + 1)).slice(-2) + "/" +
          creationDate.getFullYear() + " à " +
          ("00" + creationDate.getHours()).slice(-2) + ":" +
          ("00" + creationDate.getMinutes()).slice(-2) + ":" +
          ("00" + creationDate.getSeconds()).slice(-2);
        
          histories.push({
            id: tmp[i].id,
            userEmail: user.email,
            thingLabel: thing.label,
            itemName:item.label,
            state:tmp[i].state,
            date: creation,
          });
        }
        setHistoriques(histories);
        setIsShown(true);
      }
    }
  }

  function Home() {
    history.replace('/dashboard')
  }

  function Administration() {
    history.replace('/administration')
  }
 

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  return (
    <IonPage>
      <IonHeader>
      <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration" onClick={Administration} />
          <StyledBreadcrumb label="Historiques"/>     
        </Breadcrumbs>
      </IonHeader>
      <IonContent className="logs">

      <IonItemDivider> <IonLabel> <h1>Liste des historiques</h1> </IonLabel> </IonItemDivider>
        
        {(!isSHown) ?
          <p className="para">
            Aucune donnée
          </p>
          : null}
        {(isSHown) ?
          <TableContainer component={Paper} className="container-fluid">
            <Table className="table" size="small" aria-label="a dense table">
              <TableHead >
                <TableRow className="d-flex row">
                  <TableCell className="col-lg-2">Utilisateur</TableCell>
                  <TableCell className="col-lg-3">Objet</TableCell> 
                  <TableCell className="col-lg-3">Element</TableCell>                  
                  <TableCell className="col-lg-2">Etat</TableCell>
                  <TableCell className="col-lg-2">Date</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>
                {historiques.map((element: any) => (
                  <TableRow key={element.id} className="d-flex row">
                    <TableCell className="col-lg-2">{element.userEmail}</TableCell>
                    <TableCell className="col-lg-3">{element.thingLabel}</TableCell>                     
                    <TableCell className="col-lg-3">{element.itemName}</TableCell>                  
                    <TableCell className="col-lg-2">{element.state}</TableCell>
                    <TableCell className="col-lg-2">{element.date}</TableCell>                  
                    
                  </TableRow>
                ))}
              </TableBody>
            </Table>
           
          </TableContainer> : null}
          </IonContent>
    </IonPage >
  );
};

export default HistoryLogs;
