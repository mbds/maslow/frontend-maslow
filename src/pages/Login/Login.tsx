import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonInput, IonButton, IonLoading, IonLabel, IonAlert } from '@ionic/react';
import React, { useState } from 'react';
import './Login.css';
import {useHistory } from 'react-router-dom';
import { Toast } from '../../toast'
import Button from '@material-ui/core/Button';

import { SetSessionToken , SetSessionUser } from '../../session'


import { FirstLogin } from '../../services/auth.service'
import { CurrentUser } from '../../services/users.service'
import { TextField } from '@material-ui/core';

const Login: React.FC = () => {
 
  const [username, setUsername] = useState('') // '' initialise
  const [password, setPassword] = useState('')

  const history = useHistory()
  const [busy, setbusy] = useState<boolean>(false)
  const [isSHown, setIsShown] = useState(false);
  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };
  const handleChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };
  async function login() {
    setbusy(true)
    const token: any = await FirstLogin(username, password)

    if (token) {

      SetSessionToken(token)      
      const user : any = await CurrentUser(token)
      SetSessionUser(user)
      // faire la vérification pour passer soit en administration ou  dashboard
      history.replace('/dashboard');
      setIsShown(false);
      Toast('Login succes')
    }
    else {
      setIsShown(true);
    }

    setbusy(false)

  }

  return (
    <IonPage>

      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>
            <span>Bienvenu chez MASLOW</span>
          </IonTitle>
        </IonToolbar>
      </IonHeader> 

      <IonLoading message="Connextion en cours ... " duration={0} isOpen={busy} />

      <IonContent>
        <div className="div">
          <h2>Connexion</h2>
          <TextField
            className="textField"
            id="outlined-margin-normal roleId"
            margin="normal"
            style={{ margin: 8 }}
            onChange={handleChangeEmail}
            label="Email"
            variant="outlined"
            required
          />
          <br></br>
          <TextField
            className="textField"
            id="outlined-margin-normal roleId"
            margin="normal"
            style={{ margin: 8 }}
            onChange={handleChangePassword}
            label="Mot de passe"
            type="password"
            variant="outlined"
            required
          />
          <br></br>
          <IonLabel><a>Mot de passe oublié ?</a></IonLabel>
          <br></br>
          <IonButton className="bouton" color="secondary" onClick={login} type="submit">
            Se connecter
          </IonButton>
          </div>
        <IonAlert
          isOpen={isSHown}
          onDidDismiss={() => setIsShown(false)}
          header={'Alert'}
          message={'Email ou mot de passe incorrect !'}
          buttons={['OK']}
        />
      </IonContent>

    </IonPage>
  );
};

export default Login;
