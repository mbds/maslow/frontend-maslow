import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton, IonRow, IonGrid, IonCol, IonAlert, IonMenu, IonMenuButton, IonItemDivider, IonLabel
} from '@ionic/react';


import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import React, { useState, useEffect, useImperativeHandle } from 'react';
import './Roles.css';
import { useHistory } from 'react-router';
import { GetSessionToken, GetSessionUser } from '../../session';
import { LogoutUser } from '../../services/users.service';
import { Toast } from '../../toast';
import { deleteRole, getListRoles } from '../../services/roles.service';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles, createStyles, Theme, emphasize, withStyles  } from '@material-ui/core/styles';


const Roles: React.FC = () => {

  const token: any = GetSessionToken()
  const user: any = GetSessionUser()
  const [roles, setRoles] = useState<any>([])
  const [isSHown, setIsShown] = useState(false);
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [showDeleteNotOK, setShowDeleteNotOK] = useState(false);
  const history = useHistory();
  const [busy, setbusy] = useState<boolean>(false)


  // pour mettre async
  useEffect(() => {
    getRoles();
  }, []);

  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin:theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591
  
  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  async function getRoles() {
    if (token) {
      let tmp = await getListRoles(token);
      if (tmp.length <= 0) {
        setIsShown(false);
      }
      else {
        setRoles(tmp);
        setIsShown(true);
      }
    }
  }

  function Home() {
    Toast('Affichage Page Dashboard en cours ...')
    history.replace('/dashboard')
  }

  function Administration() {
    history.replace('/administration')
  }

  function RoleForm() {
    history.replace('/roleForm')
  }

  function Detail(id: string) {
    history.replace('/roledetails/' + id)

  }

  async function Supprimer(id: string) {
    if (deleteRole(token, id)) {
      await sleep(100);
      getRoles();
    }
    else {
      setShowDeleteNotOK(true);
    }

  }


  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  return (
    <IonPage>
      <IonHeader>
      <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration" onClick={Administration} />
          <StyledBreadcrumb label="Rôles"/>     
        </Breadcrumbs>
      </IonHeader>
      <IonContent className="roles">
      <IonItemDivider> <IonLabel> <h1>Liste des rôles</h1> </IonLabel> </IonItemDivider>
       
        {(!isSHown) ?
          <p className="para">
            Aucune donnée
          </p>
          : null}
        {(isSHown) ?
          <TableContainer component={Paper} className="container-fluid">
            <Table className="table" size="small" aria-label="a dense table">
              <TableHead >
                <TableRow className="d-flex row">
                  <TableCell className="col-lg-3">ID</TableCell>
                  <TableCell className="col-lg-3">Nom</TableCell>
                  <TableCell className="col-lg-4">Description</TableCell>
                  <TableCell className="col-lg-2">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {roles.map((element: any) => (
                  <TableRow key={element.id} className="d-flex row">
                    <TableCell className="col-lg-3" scope="row">
                      {element.id}
                    </TableCell>
                    <TableCell className="col-lg-3">{element.name}</TableCell>
                    <TableCell className="col-lg-4">{element.description}</TableCell>
                    <TableCell className="col-lg-2">
                      <a onClick={() => Detail(element.id)}   >
                        <img className="image" src="assets/images/modifier.png" title="Modifier un utilisateur" />
                      </a>
                      <a onClick={() => setShowDeleteAlert(true)}   >
                        <img className="image" src="assets/images/supprimer.png" title="Supprimer un utilisateur" />
                      </a>
                    </TableCell>
                    <IonAlert
                      isOpen={showDeleteAlert}
                      onDidDismiss={() => setShowDeleteAlert(false)}
                      header={'Êtes-vous sûr ?'}
                      message={'Vous allez supprimer ce rôle définitivement !'}
                      buttons={[
                        {
                          text: 'Supprimer',
                          handler: () => {
                            Supprimer(element.id);
                          }
                        },
                        {
                          text: 'Annuler',
                          handler: () => {
                          }
                        }
                      ]}

                    />
                    <IonAlert
                      isOpen={showDeleteNotOK}
                      onDidDismiss={() => setShowDeleteNotOK(false)}
                      header={'Alert'}
                      message={"Une erreur s'est produite lors de la supression du rôle !"}
                      buttons={['OK']}
                    />
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer> : null}
          <IonButton className="bouton" color="secondary" type="submit" onClick={RoleForm}>
            Ajouter Role
          </IonButton>
        </IonContent>
    </IonPage >
  );
};

export default Roles;
