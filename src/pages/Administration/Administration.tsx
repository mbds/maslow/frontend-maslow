import {
  IonContent, IonHeader, IonPage, IonButton} from '@ionic/react';

import Button from '@material-ui/core/Button';
import React, { useState, useEffect } from 'react';
import './Administration.css';
import { useHistory } from 'react-router';

import { GetSessionUser } from '../../session'
import { LogoutUser } from '../../services/users.service'

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown'
import { makeStyles, createStyles, Theme, emphasize, withStyles  } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { } from '@material-ui/core/styles';

const Administration: React.FC = () => {
  const history = useHistory()
  const [, setbusy] = useState<boolean>(false)
  // pour mettre async
  useEffect(() => {
  }, []);


  const user: any = GetSessionUser()
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
    }),
  );
  const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
      //backgroundColor: theme.palette.grey[100],
      height: theme.spacing(3),
      color: theme.palette.grey[800],
      fontWeight: theme.typography.fontWeightRegular,
      margin:theme.spacing(1),
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[300],
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: emphasize(theme.palette.grey[300], 0.12),
      },
    },
    label: {
      margin:0
    }
  }))(Chip) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591
  
  const classes = useStyles();


  function Users() {
    history.replace('/users')
  }

  function Privileges() {
    history.replace('/privileges')
  }

  function Roles() {
    history.replace('/roles')
  }

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  function Home() {
    history.replace('/dashboard')
  }

  function Administration() {
    history.replace('/administration')
  }

  function HistoryLogs() {
    history.replace('/historyLogs')
  }
  
  return (
    <IonPage>
      <IonHeader>
        <Navbar bg="secondary" expand="lg" className="navbar d-flex justify-content-between">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Item>
                <Nav.Link onClick={Home} className="m-link">Accueil</Nav.Link>
              </Nav.Item>
              {(user.roleId=="ADMIN") ?
              <Nav.Item>
                <Nav.Link onClick={Administration} className="m-link active">Administration</Nav.Link>
              </Nav.Item>
              :null}
              {(user) ?
                <Nav.Item className=" profil m-link">
                  <NavDropdown title="Profil" className="dropleft" id="">
                    <NavDropdown.Item disabled>{user.firstname} {user.lastname}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item disabled>{user.email}</NavDropdown.Item>
                    <NavDropdown.Item disabled>{user.roleId}</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item>
                      <IonButton color="secondary" onClick={logout}>
                          Se déconnecter
                      </IonButton>
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav.Item>
                : null}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Breadcrumbs className="Breadcrumbs" aria-label="breadcrumb">
          <StyledBreadcrumb
            label="Home"
            icon={<HomeIcon fontSize="small" />}
            onClick={Home}
          />
          <StyledBreadcrumb label="Administration"/>
          
        </Breadcrumbs>
      </IonHeader>
      <IonContent>
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid className="grid" item xs={6} sm={3}>
              <a target="_blank" href="http://localhost:8080/habpanel/index.html#/" >
                <div className={classes.paper}>
                  <img className="iconAdministration" src="assets/images/sitemaps.png" /><br></br>
                  <span className="title">Sitemaps/Objets</span><hr></hr>
                  <span className="description">Pour gerer tous vos objets connectés et sitemaps</span>
                </div>
              </a>
            </Grid>
            <Grid className="grid" item xs={6} sm={3}>
              <a onClick={Users} >
                <div className={classes.paper}>
                  <img className="iconAdministration" src="assets/images/users.png" /><br></br>
                  <span className="title">Utilisateurs</span><hr></hr>
                  <span className="description">Pour gerer les utilisateurs</span>

                </div>
              </a>
            </Grid>
            <Grid className="grid" item xs={6} sm={3}>
              <a onClick={Privileges} >
                <div className={classes.paper}>
                  <img className="iconAdministration" id="btnLeft" src="assets/images/groups.png" /><br></br>
                  <span className="title">Priviléges</span><hr></hr>
                  <span className="description">Pour gerer les priviléges</span>
                </div>
              </a>
            </Grid>
            <Grid className="grid" item xs={6} sm={3}>
              <a onClick={Roles} >
                <div className={classes.paper}>
                  <img className="iconAdministration" src="assets/images/role.png" /><br></br>
                  <span className="title">Rôles</span><hr></hr>
                  <span className="description">Pour gerer les rôles</span>
                </div>
              </a>
            </Grid>
            <Grid className="grid" item xs={6} sm={3}>
              <a onClick={HistoryLogs} >
                <div className={classes.paper}>
                  <img className="iconAdministration" src="assets/images/history.jpg" /><br></br>
                  <span className="title">Historiques</span><hr></hr>
                  <span className="description">Pour avoir une vue sur les historiques de controles des objets connectés</span>
                </div>
              </a>
            </Grid>
          </Grid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Administration;
