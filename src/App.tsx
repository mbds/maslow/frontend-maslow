import React, { useEffect, useState } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, IonSpinner } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home/Home';
import NotFound from './pages/NotFound/NotFound'
import Dashboard from './pages/Dashboard/Dashboard'

import Users from './pages/Users/Users'
import Login from './pages/Login/Login'
import Items from './pages/Items/Items'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';


import { useDispatch } from 'react-redux';
import { setUserState } from './redux/action';

import {GetSessionToken  , GetSessionUser} from './session'
import ThingAccess from './pages/ThingAccess/ThingAccess';
import UserForm from './pages/UserForm/UserForm';
import Administration from './pages/Administration/Administration';
import UserDetails from './pages/UserDetails/UserDetails';
import RoleDetails from './pages/RoleDetails/RoleDetails';
import Roles from './pages/Roles/Roles';
import RoleForm from './pages/RoleForm/RoleForm';
import HistoryLogs from './pages/HistoryLogs/HistoryLogs';
import ThingAccessDetails from './pages/ThingAccessDetails/ThingAccessDetails';
import Privileges from './pages/Privileges/Privileges';


const RoutingSystem: React.FC = () => {
  return (
    <IonReactRouter>
      <IonRouterOutlet>
        {/* exact sert en cas de /pages/page1 pour qu'il affiche exactement page1 et non pages */}
        <Route path="/home" component={Home} exact={true} />           
        <Route path="/login" component={Login} exact={true} />
        <Route path="/accesses/:id" component={ThingAccess} exact={true} />
        <Route path="/thingaccess/:id" component={ThingAccessDetails} exact={true} />        
        <Route path="/items/:thingUid" component={Items} exact={true} />
        <Route path="/dashboard" component={Dashboard} exact={true} />
        <Route path="/administration" component={Administration} exact={true} />  
        <Route path="/users" component={Users} exact={true} />
        <Route path="/privileges" component={Privileges} exact={true} />                       
        <Route path="/historyLogs" component={HistoryLogs} exact={true} />     
        <Route path="/userForm" component={UserForm} exact={true} />          
        <Route path="/userdetails/:id" component={UserDetails} exact={true} />   
        <Route path="/roles" component={Roles} exact={true} /> 
        <Route path="/roleForm" component={RoleForm} exact={true} />        
        <Route path="/roledetails/:id" component={RoleDetails} exact={true} /> 
        
        <Route path="*" component={NotFound} exact={true} />
        <Route path="/" component={Home} exact={true} />
        <Route exact path="/" render={() => <Redirect to="/home" />} />
      </IonRouterOutlet>
    </IonReactRouter>
  )
}


const App: React.FC = () => {

  const [busy, setbusy] = useState<boolean>(true)
  const dispatch = useDispatch()

  useEffect(() => {
                      
      if (GetSessionUser() !== null) {               
        window.history.replaceState({}, '', '/dashboard')
      } else {
        window.history.replaceState({}, '', '/')
      }
      
      setbusy(false)
    
  }, [])

  return (        
    <IonApp>      
      {busy ? <IonSpinner /> :  <RoutingSystem />}
    </IonApp>
  )
}

export default App;
