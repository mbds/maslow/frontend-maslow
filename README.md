# frontend-maslow


MASLOW rend la vie meilleure, améliore la qualité, réduit la consommation, améliore l'efficacité de la gestion, les économies de coûts et le monitoring.

L'objectif de ce projet est de mettre en place une maison intelligente. Une maison intelligente désigne une installation domestique pratique où les appareils et les dispositifs peuvent être automatiquement contrôlés à distance, de n'importe où, grâce à une connexion internet utilisant un appareil mobile ou un autre dispositif en réseau. Les appareils d'une maison intelligente sont interconnectés par l'internet, ce qui permet à l'utilisateur de contrôler à distance des fonctions telles que la sécurité de l'accès à la maison, la température et l'éclairage.

Etudiante :
*   BOUKADIDA Imen 

Etapes pour executer le front-end:

1.  Faire un clone du repository front-end
2.  Ouvrir un terminal et se diriger vers le dossier contenant le projet cloné
3.  Executer les commandes suivantes:
    * npm install @ionic/CLI
    * npm install
    * npm serve
4.  Lancer localhost:8100
5.  Sur ce site vous pouvez trouver l'interface d'interaction avec notre système
6.  Pour vous connecter en tant que : 
    * Administrateur -> email : admin@maslow.mbds.fr || mot de passe : adminpassword
    * Simple utilisateur -> email : user@maslow.mbds.fr || mot de passe : userpassword


**L'équipe MASLOW**